<?php
  require_once( "./comum.php");
  require_once( BASE_DIR . "/classes/Produto.php");
  //require_once( BASE_DIR . "/classes/ExcluirDao.php");

  session_start();
  // if( !isset( $_SESSION["usuario"] ) )
  // {
    // Header("location: inicio.php");
  // }
  
  $categorias = Produto::findAll();
  
?>
<html>
<head>
  <meta charset="UTF-8" />
  <title>Bazar Tem Tudo</title>
</head>
<body>
  <script>
    
  </script>
  <?php require_once("cabecalho.inc"); ?>

  <div id="corpo">
    <table border="1">
      <thead>
        <tr>
          <th>Código</th>
          <th>Descrição</th>
          <th>Taxa</th>
        </tr>
      </thead>
      <tbody>
        <?php
        foreach( $categorias as $produto) {
        ?>
        <tr>
          <td><?= $produto->getIdProduto() ?></td>
          <td><?= $produto->getDescricao() ?></td>
          <td><?= $produto->getId_usuario() ?></td>
          <td>
            <form action="excluir_categoria.php" method="post">
              <input type="hidden" name="id" value="<?php echo $produto->getIdCategoria() ?>">
              <input type="hidden" name="descricao" value="<?php echo $produto->getDescricao() ?>">
              <button type="submit">Excluir</button>
            </form>
            
            <form action="form_altera_categoria.php" method="get">
              <input type="hidden" name="id" value="<?php echo $produto->getIdCategoria() ?>">
              <input type="hidden" name="descricao" value="<?php echo $produto->getDescricao() ?>">
              <input type="hidden" name="taxa" value="<?php echo $produto->getTaxa() ?>">
              <button type="submit">Alterar</button>
            </form>
          </td>
        </tr>
        <?php
        }
        ?>
      </tbody>
    </table>
    <div>
      <a href="form_categoria.php">Incluir categoria</a>
    </div>
  </div>

  <?php require_once("rodape.inc"); ?>

</body>
</html>
