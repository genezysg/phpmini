<?php
require_once( BASE_DIR . "/classes/UsuarioDao.php");

class Usuario
{
  use UsuarioDao;

  private $nome;
  private $senha;
  private $id_nome;

  public function __construct( $nome, $senha, $id_nome)
  {
    $this->nome = $nome;
    $this->descricao = $senha;
    $this->id_usuario = $id_nome;
  }

  public function getNome()
  {
    return $this->nome;
  }

  public function getSenha()
  {
    return $this->senha;
  }

  public function getId_nome()
  {
    return $this->id_nome;
  }
}
