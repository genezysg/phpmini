<?php
require_once( BASE_DIR . "/classes/Banco.php");

trait ProdutoDao
{
  public static function rowMapper($produto, $descricao, $id_usuario,$id_produto)
  {
    return new Produto( $produto, $descricao, $id_usuario, $id_produto);
  }

  public static function findAll()
  {
      $pdo = Banco::obterConexao();
      $statement = $pdo->prepare("SELECT produto, descricao, nome,id_produto FROM produto, usuario WHERE id_usuario = id_nome");
      $statement->execute();
      /*
      return $statement->fetchAll( PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE,
      "Categoria", array( 'xxxx', 'xxx', 'xxx') );
      */

      return $statement->fetchAll( PDO::FETCH_FUNC, "ProdutoDao::rowMapper" );
  }

  public static function buscaProduto($id_prod)
  {
      $pdo = Banco::obterConexao();

	  $sql = ("SELECT produto, descricao, nome,id_produto FROM produto, usuario WHERE id_usuario = id_nome and id_produto =:id_prod");

		$statement = $pdo->prepare($sql);
		$statement->bindValue(':id_prod', $id_prod, PDO::PARAM_INT);

		$statement->execute();

		$registro = $statement->fetch(PDO::FETCH_ASSOC);

		return $registro; // array associativo
    // Ao retornar como array associativo, o registro volta como um array onde as chaves são os nomes das tabelas, para acessar os campos

  }


}
