<?php
require_once( BASE_DIR . "/classes/ProdutoDao.php");

class Produto
{
  use ProdutoDao;

  private $produto;
  private $descricao;
  private $id_usuario;
  private $id_produto;
  
  public function __construct( $produto, $descricao, $id_usuario,$id_produto )
  {
    $this->produto = $produto;
    $this->descricao = $descricao;
    $this->id_usuario = $id_usuario;
	$this->id_produto = $id_produto;
  }

  public function getProduto()
  {
    return $this->produto;
  }

  public function getDescricao()
  {
    return $this->descricao;
  }

  public function getId_usuario()
  {
    return $this->id_usuario;
  }
  public function getId_produto()
  {
	return $this->id_produto;
  }
}