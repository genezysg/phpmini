<?php
require_once( BASE_DIR . "/classes/Banco.php");

trait UsuarioDao
{
  public static function rowMapper($nome, $senha, $id_nome)
  {
    return new Usuario( $nome, $senha, $id_nome);
  }

  public static function findAll()
  {
      $pdo = Banco::obterConexao();
      $statement = $pdo->prepare("SELECT produto, descricao, nome FROM produto, usuario WHERE id_usuario = id_nome");
      $statement->execute();
      /*
      return $statement->fetchAll( PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE,
      "Categoria", array( 'xxxx', 'xxx', 'xxx') );
      */

      return $statement->fetchAll( PDO::FETCH_FUNC, "UsuarioDao::rowMapper" );
  }
}
